use aws_lambda_events::event::s3::S3Event;
use aws_sdk_s3::{Client, Region};
use lambda_runtime::{run, service_fn, Error, LambdaEvent};


async fn handler(event: LambdaEvent<S3Event>) -> Result<(), Error> {
    let bucket_name = std::env::var("BUCKET_NAME")
    .expect("A BUCKET_NAME must be set in this app's Lambda environment variables.");



    let shared_config = aws_config::from_env().region(Region::new("eu-west-1")).load().await;
    let client = Client::new(&shared_config);

    let uploaded_file_name = event.payload.records[0].s3.object.key.as_ref().expect("no file");

    let content = client.get_object().bucket(bucket_name).key(uploaded_file_name).send().await.unwrap();

    println!("file name = {} content: {:?}", uploaded_file_name, content);

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt::init();

    run(service_fn(handler)).await
}
