import * as cdk from 'aws-cdk-lib';
import { LambdaIntegration, RestApi } from 'aws-cdk-lib/aws-apigateway';
import { Bucket, EventType } from 'aws-cdk-lib/aws-s3';

import { Code, Runtime } from 'aws-cdk-lib/aws-lambda';
import { Construct } from 'constructs';
import {join} from 'path'
import { RustLambda } from './rust-lambda-construct';
import { NodejsFunction } from 'aws-cdk-lib/aws-lambda-nodejs';
import { LambdaDestination } from 'aws-cdk-lib/aws-s3-notifications';
import { RemovalPolicy } from 'aws-cdk-lib';


const getRustAssetsPath = (assetName: string) => {
  return join(__dirname, `../../../target/lambda/${assetName}/bootstrap.zip`)
}


export class InfrastructureStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);
    const testHandler = new RustLambda(this, 'rustLambdaHonestly', {
      code: Code.fromAsset(getRustAssetsPath('apigatewaytest')),
      description: 'just a lambda',
      environment: {
        ENV_PROP: 'SAY HELLO TO MY LITTLE FRIEND CDK'
      }
    })

    

    const rustBucket = new Bucket(this, 'rustBucket', {autoDeleteObjects: true, removalPolicy: RemovalPolicy.DESTROY})
    const nodeBucket = new Bucket(this, 'nodeBucket', {autoDeleteObjects: true, removalPolicy: RemovalPolicy.DESTROY})

    const rustFileReaderHandler = new RustLambda(this, 'rustReaderHonestly', {
      code: Code.fromAsset(getRustAssetsPath('readfilerust')),
      environment: {
        BUCKET_NAME: rustBucket.bucketName
      }
    })

    const nodeFileHandler = new NodejsFunction(
      this,
      'nodeFileHandler',
      {
        entry: join(__dirname, './lambdas/readFileNode.ts'),
        handler: 'handler',
        environment: {
          BUCKET_NAME: nodeBucket.bucketName,
        },
      }
    )

    
    rustBucket.grantRead(rustFileReaderHandler)
    nodeBucket.grantRead(nodeFileHandler)
    const nodeDest = new LambdaDestination(nodeFileHandler)
    const rustDest = new LambdaDestination(rustFileReaderHandler)

    rustBucket.addEventNotification(EventType.OBJECT_CREATED,  rustDest)
    nodeBucket.addEventNotification(EventType.OBJECT_CREATED, nodeDest)

    

    const api = new RestApi(this, 'api', {restApiName: 'test'})
  
    const testRoot = api.root.addResource('test')

    const rustRoot = api.root.addResource('rust')
    const rustSignatureHandler = new RustLambda(this, 'signatureHandlerRust', {
      code: Code.fromAsset(getRustAssetsPath('rustsignaturehandler')),
      environment: {
        BUCKET_NAME: rustBucket.bucketName
      }
    })

    rustBucket.grantWrite(rustSignatureHandler)

    const nodeSignatureHandler = new NodejsFunction(
      this,
      'sustainabilityLambda',
      {
        entry: join(__dirname, './lambdas/signatureHandlerNode.ts'),
        handler: 'handler',
        environment: {
          BUCKET_NAME: nodeBucket.bucketName,
        },
      }
    )

    nodeBucket.grantWrite(nodeSignatureHandler)

    rustRoot.addMethod('GET', new LambdaIntegration(rustSignatureHandler, {proxy: true}))



    const nodeRoot = api.root.addResource('node')
    nodeRoot.addMethod('GET', new LambdaIntegration(nodeSignatureHandler, {proxy: true}))

    testRoot.addMethod('GET',  new LambdaIntegration(testHandler, {proxy: true}))
  }
}
