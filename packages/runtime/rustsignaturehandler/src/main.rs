use std::time::Duration;
use chrono::{ Utc, Timelike};

use lambda_http::{Body, Error, Request, Response, run, service_fn};
use aws_sdk_s3::{Client, Region, presigning::config::PresigningConfig};



async fn put_object(
    client: &Client,
    bucket: &str,
) -> Result<String, Error> {
    let expires_in = Duration::from_secs(360);
    let now = Utc::now();
    let now_string = format!("{}{}",  now.minute(),
    now.second()).to_string();
    let presigned_request = client
        .put_object()
        .key(now_string)
        .bucket(bucket)
        .presigned(PresigningConfig::expires_in(expires_in)?)
        .await?;

    Ok(presigned_request.uri().to_string())
}

async fn handler(event: Request) -> Result<Response<Body>, Error> {
    let bucket_name = std::env::var("BUCKET_NAME")
    .expect("A BUCKET_NAME must be set in this app's Lambda environment variables.");

    let shared_config = aws_config::from_env().region(Region::new("eu-west-1")).load().await;
    let client = Client::new(&shared_config);

    let upload_link = put_object(&client, &bucket_name).await.expect("No Link");

    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/html")
        .body(upload_link.into())
        .map_err(Box::new)?;
    Ok(resp)
}


#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt::init();
    

    run(service_fn(handler)).await
}