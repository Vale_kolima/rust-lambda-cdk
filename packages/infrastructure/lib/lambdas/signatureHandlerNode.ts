import { S3Client, PutObjectCommand } from '@aws-sdk/client-s3'
import { getSignedUrl } from '@aws-sdk/s3-request-presigner'

import type { APIGatewayProxyHandler } from 'aws-lambda'
import { DateTime } from 'luxon'

export const handler: APIGatewayProxyHandler =
  async event => {
    try {
      const { BUCKET_NAME } = process.env

      if (!BUCKET_NAME) {
        throw new Error('No bucket name available')
      }
      const now = DateTime.now()
      const Key = now.toFormat('FF')


      const command = new PutObjectCommand({
        Key,
        Bucket: BUCKET_NAME,
      })
      const signedUrl = await getSignedUrl(new S3Client({}), command)

      return {
        statusCode: 201,
        body: JSON.stringify({
          signedUrl,
          Key,
        }),
      }
    } catch (error) {
      console.log(error)
      return {
        statusCode: 500,
        body:
          error instanceof Error ? error.message : 'There has been an issue',
      }
    }
  }
