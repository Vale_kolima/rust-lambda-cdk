import {S3Handler } from 'aws-lambda'
import { S3Client, GetObjectCommand } from '@aws-sdk/client-s3'


export const handler: S3Handler = async event => {
    const client = new S3Client({region: 'eu-west-1'})
    const { BUCKET_NAME } = process.env

      if (!BUCKET_NAME) {
        throw new Error('No bucket name available')
      }

    const Key = event.Records[0].s3.object.key
    const getObjectCommand  = new GetObjectCommand({Key, Bucket: BUCKET_NAME})

    try {
        const response = await client.send(getObjectCommand)
    
        const content = await response.Body?.transformToString('utf-8')

        console.log(`fileName ${Key} with content ${content}`, 'NODE')
      } catch (err) {
        throw new Error('nope soz')
      } 
  
} 