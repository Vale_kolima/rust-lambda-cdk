import { Architecture, Function, FunctionProps, Runtime }from 'aws-cdk-lib/aws-lambda'
import { Construct } from 'constructs'

export class RustLambda extends Function {
    constructor(ctx: Construct, id: string, props: Pick<FunctionProps, 'code' | 'description' | 'environment'>){
        super(ctx, id, {
            handler: 'not.required',
            runtime: Runtime.PROVIDED_AL2,
            architecture: Architecture.ARM_64,
            description: props.description,
            code: props.code,
            environment: props.environment
        })
    }
}