# Interop RUST lambda with typescript CDK

The goal of this POC is to prove an interop the performance that we could gain by using a Rust as a lambda runtime, but at the same time
without excluding the possiblity of having a side by side implementation with the current way of work with NodeJS and Typescript CDK

# How to

At the moment the rust runtimes are creating using the AWS lab cli [https://github.com/awslabs/aws-lambda-rust-runtime]
and the orchestrating of the the compile commands and creation is done via Yarn workspaces.

- yarn new:lambda to create a new lambda
- yarn deploy ( to run the subcommand build command in the rust envs `cargo lambda build --release --bin apigatewaytest --arm64 --output-format zip`)

the cdk then work as usual, there is just a construct to facilitate and abstract some properties while the runtime off rust get imported as an assets inside the lambda used
to reply to the gateway in the usual manner.


# Performance diff between node I/O and Rust I/O
![Node perf](./imgs/nodePerf.png "Node perf")
![Rust perf](./imgs/rustPerf.png "Rust perf")


# TODO:

- Programmatically pull performance out of cloudwatch during integration test and build integration.